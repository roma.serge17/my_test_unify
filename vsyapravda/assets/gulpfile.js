// Gulp File

const gulp = require("gulp");
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');



const patchs = {
	cleanCSS: {
		src: "./build/*",
		build: "./build"
	},
	css: {
		src: "./src/css/**/*.css",
		build: "./build/css/"
	},
	js: {
		src: "./src/js/**/**/*.js",
		build: "./build/js/"
	}
	

};


const moveCLEAN = () => 
gulp.src(patchs.cleanCSS.src).
pipe(clean({
	level: 2
})).
pipe(gulp.dest(patchs.cleanCSS.build));

const moveCSS = () => 
gulp.src(patchs.css.src).
pipe(cleanCSS({compatibility: 'ie8'})).
pipe(concat('main.min.css')).
pipe(autoprefixer({
	overrideBrowserslist: ['last 2 versions'],
	cascade: false
})).
pipe(gulp.dest(patchs.css.build));

const moveJS = () => 
gulp.src(patchs.js.src).
pipe(concat('script.min.js')).
pipe(uglify()).
pipe(gulp.dest(patchs.js.build));
const watch = () => {

	gulp.watch(patchs.cleanCSS.src, moveCLEAN);
	gulp.watch(patchs.css.src, moveCSS);
	gulp.watch(patchs.js.src, moveJS);

				

}

gulp.task('build', gulp.series(moveCLEAN, gulp.parallel(moveCSS, moveJS)));

gulp.task('dev', gulp.series('build', watch));




